import java.util.ArrayList;
import java.util.List;

public class DummyReceiver {

  enum eSection {
    INVALID, EMPTY, TOP, NET, USB, DLNA, DISC, MUSIC, FOLDER, SUBFOLDER1, SUBFOLDER2, SUBFOLDER3, PLAYING
  }

  private class Section {
    eSection type;
    int pageIndex;


    Section(eSection type, int pageIndex) {
      this.type = type;
      this.pageIndex = pageIndex;
    }
  }


  private volatile List<Section> mSectionStack;
  private int mCursorPos;
  private int mPageIndex;
  private int mPlayingSeconds;
  private int mPlayingTrack;
  private boolean mIsPlaying;


  public DummyReceiver() {
    mSectionStack = new ArrayList<Section>();
    pushSection(eSection.TOP);
  }


  public void run() {
    while (true) {
      //System.out.println((getCurrentSection() == eSection.PLAYING)+" "+mIsPlaying);
      if (getCurrentSection() == eSection.PLAYING && mIsPlaying) {
        sendCommand("NTM" + secondsToString(mPlayingSeconds) + "/05:39");
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {}
        mPlayingSeconds++;
      }
      Thread.yield();
    }
  }


  public void onCommand(String command) {
    if (command.compareTo("PWRQSTN") == 0) {
      sendCommand("PWR01");
    }

    if (command.compareTo("SLI2B") == 0) {
      clearSections();
      pushSection(eSection.NET);
      sendCommand("SLI2B");
    }

    if (command.compareTo("SLI29") == 0) {
      clearSections();
      pushSection(eSection.USB);
      sendCommand("SLI29");
    }

    if (command.compareTo("NSV00") == 0) {
      if (getCurrentSection() == eSection.NET) {
        pushSection(eSection.DLNA);
      }
    }

    if (command.compareTo("NTCSELECT") == 0) {
      onItemClicked();
    }

    if (command.compareTo("NTCRETURN") == 0) {
      popSection();
    }

    if (command.compareTo("NTCSTOP") == 0) {
      mIsPlaying = false;
      mPlayingSeconds = 0;
      sendCommand("NSTSR-");
    }

    if (command.compareTo("NTCPLAY") == 0) {
      if (getCurrentSection() == eSection.PLAYING) {
        mIsPlaying = true;
        sendCommand("NSTPR-");
      }
    }

    if (command.compareTo("NSTQSTN") == 0) {
      if (getCurrentSection() == eSection.PLAYING && mIsPlaying) {
        sendCommand("NSTPR-");
      } else {
        sendCommand("NSTSR-");
      }
    }

    if (command.compareTo("NTCTOP") == 0) {
      if (mSectionStack.size() > 0 && mSectionStack.get(0).type == eSection.NET) {
        clearSections();
        pushSection(eSection.NET);
        pushSection(eSection.DLNA);
      } else if (mSectionStack.size() > 0 && mSectionStack.get(0).type == eSection.USB) {
        clearSections();
        pushSection(eSection.USB);
      }
    }

    if (command.compareTo("NTCUP") == 0) {
      String[] items = getSectionItems(getCurrentSection());
      if (items != null) {
        mCursorPos--;
        boolean pageChange = false;
        if (mCursorPos < 0) {
          if (mPageIndex > 0) {
            mPageIndex--;
            mCursorPos = 9;
            pageChange = true;
          } else if (items.length <= 10) {
            mCursorPos = items.length - 1;
          } else {
            mPageIndex = (items.length - 1) / 10;
            mCursorPos = (items.length - 1) % 10;
            pageChange = true;
          }
        }
        if (pageChange) {
          listCurrentSection();
        } else {
          sendCommand("NLSC" + mCursorPos + "C");
        }
      }
    }

    if (command.compareTo("NTCDOWN") == 0) {
      String[] items = getSectionItems(getCurrentSection());
      if (items != null) {
        mCursorPos++;
        boolean pageChange = false;
        if (mCursorPos >= 10 || mPageIndex * 10 + mCursorPos >= items.length) {
          if (mPageIndex < (items.length - 1) / 10) {
            mPageIndex++;
            mCursorPos = 0;
            pageChange = true;
          } else if (items.length <= 10) {
            mCursorPos = 0;
          } else {
            mPageIndex = 0;
            mCursorPos = 0;
            pageChange = true;
          }
        }
        if (pageChange) {
          listCurrentSection();
        } else {
          sendCommand("NLSC" + mCursorPos + "C");
        }
      }
    }

    if (command.startsWith("NLSL")) {
      int index = Integer.valueOf(command.substring(4));
      String[] items = getSectionItems(getCurrentSection());
      if (items != null && index >= 0 && index < items.length) {
        mCursorPos = index % 10;
        onItemClicked();
      }
    }

    if (command.startsWith("NTCTR")) {
      String[] items = getSectionItems(mSectionStack.get(mSectionStack.size() - 2).type);
      if (command.compareTo("NTCTRDN") == 0) {
        if (--mPlayingTrack == -1) {
          mPlayingTrack = items.length - 1;
        }
      } else {
        mPlayingTrack = (mPlayingTrack + 1) % items.length;
      }
      mPlayingSeconds = 0;
      listCurrentSection();
    }
  }


  private void onItemClicked() {
    switch (getCurrentSection()) {
      case EMPTY :
        popSection();
        break;
      case NET :
        if (getItemIndex() == 4) {
          pushSection(eSection.DLNA);
        }
        break;
      case USB :
        if (getItemIndex() == 0) {
          pushSection(eSection.FOLDER);
        }
        break;
      case DLNA :
        pushSection(eSection.DISC);
        break;
      case DISC :
        if (getItemIndex() == 0) {
          pushSection(eSection.MUSIC);
        }
        break;
      case MUSIC :
        if (getItemIndex() == 6) {
          pushSection(eSection.FOLDER);
        }
        break;
      case FOLDER :
        pushSection(eSection.values()[eSection.SUBFOLDER1.ordinal() + getItemIndex()]);
        break;
      case SUBFOLDER1 :
      case SUBFOLDER2 :
      case SUBFOLDER3 :
        mPlayingTrack = getItemIndex();
        pushSection(eSection.PLAYING);
        break;
    }
  }


  private String[] getSectionItems(eSection section) {
    String[] items = null;
    switch (section) {
      case EMPTY :
        items = new String[]{"<back>"};
        break;
      case TOP :
        break;
      case NET :
        items = new String[]{"vTuner Internet Radio", "Last.fm Internet Radio", "Napster", "My Favorites", "DLNA"};
        break;
      case USB :
        items = new String[]{"USB Storage"};
        break;
      case DLNA :
        items = new String[]{"MyBookLive-Twonky"};
        break;
      case DISC :
        items = new String[]{"Music", "Photos", "Search"};
        break;
      case MUSIC :
        items = new String[]{"Album", "All Tracks", "Artist", "Artist Index", "Artist/Album", "Composer", "Folder",
            "Genre/Album", "Genre/Artist", "Genre/Song", "Personal rating", "Playlists"};
        break;
      case FOLDER :
        items = new String[]{"Flute Music For Meditation", "Cello", "Esraj - My Heart-Song"};
        break;
      case SUBFOLDER1 :
        items = new String[]{"Awakening", "Ami Jami", "Chaowa Paowar Shesh Habena", "Hiya Pakhi", "Chaire Rabi Bhai",
            "Bhukite Diyona", "Kanna Amar Shesh Habena", "Je Dike Phirai", "Bela Chale Jai", "Hiya Pakhi",
            "Nrittyer Tale Tale", "Tomai Dite", "Age Nahi Bale", "Ghana Tamas Abrita", "Swapan Tari Tumi Amar",
            "Debata Eseche", "Alpha Kathar"};
        break;
      case SUBFOLDER2 :
        items = new String[]{"Track 1", "Track 2", "Track 3", "Track 4", "Track 5", "Track 6", "Track 7", "Track 8",
            "Track 9", "Track 10", "Track 11", "Track 12", "Track 13", "Track 14", "Track 15", "Track 16", "Track 17",
            "Track 18", "Track 19", "Track 20"};
        break;
      case SUBFOLDER3 :
        items = new String[]{"Esraj 1", "Esraj 2"};
        break;
    }
    return items;
  }


  private void listCurrentSection() {
    if (getCurrentSection() == eSection.PLAYING) {
      sendCommand("NLSC-P");
      sendCommand("NATguru");
      sendCommand("NALalbum");
      String[] items = getSectionItems(mSectionStack.get(mSectionStack.size() - 2).type);
      sendCommand("NTI" + items[mPlayingTrack]);
      sendCommand("NTR" + indexToString(mPlayingTrack, 4) + "/" + indexToString(items.length, 4));
      /*for (int i = 0; i < 10; i++) {
        sendCommand("NJA00ahojda");
      }*/
    }

    String[] items = getSectionItems(getCurrentSection());
    if (items != null) {
      sendCommand("NLSC" + mCursorPos + "P");
      int index = mPageIndex * 10;
      for (int i = index; i < items.length && i < index + 10; i++) {
        sendCommand("NLSU" + (i - index) + "-" + items[i]);
      }
    }
  }


  private void pushSection(eSection section) {
    synchronized (mSectionStack) {
      System.out.println("Entering " + section);
      if (section == eSection.PLAYING) {
        mPlayingSeconds = 0;
        mIsPlaying = true;
      }
      mSectionStack.add(new Section(section, mPageIndex));
      mCursorPos = 0;
      mPageIndex = 0;
      listCurrentSection();
    }
  }


  private void popSection() {
    synchronized (mSectionStack) {
      mCursorPos = 0;
      mPageIndex = 0;
      if (mSectionStack.size() > 0) {
        mPageIndex = mSectionStack.get(mSectionStack.size() - 1).pageIndex;
        mSectionStack.remove(mSectionStack.size() - 1);
        if (mSectionStack.size() > 0) {
          System.out.println("Returning to " + getCurrentSection());
        }
        listCurrentSection();
      }
    }
  }


  private void clearSections() {
    synchronized (mSectionStack) {
      mSectionStack.clear();
    }
  }


  private int getItemIndex() {
    return mPageIndex * 10 + mCursorPos;
  }


  private eSection getCurrentSection() {
    synchronized (mSectionStack) {
      if (mSectionStack.size() == 0) {
        return eSection.INVALID;
      } else {
        return mSectionStack.get(mSectionStack.size() - 1).type;
      }
    }
  }


  private String secondsToString(int seconds) {
    int minutes = seconds / 60;
    seconds = seconds % 60;
    String result = "";
    if (minutes < 10)
      result += "0";
    result += minutes;
    result += ":";
    if (seconds < 10)
      result += "0";
    result += seconds;
    return result;
  }


  private String indexToString(int index, int numplaces) {
    int validPlaces = 1;
    int num = index;
    while (num >= 10) {
      num = num / 10;
      validPlaces++;
    }
    String result = "";
    for (int i = validPlaces; i < numplaces; i++) {
      result += "0";
    }
    result += index;
    return result;
  }


  private void sendCommand(String command) {
    TestServer.sendCommand(command);
  }

}
