
public class CommandParser {

  public static byte[] commandToPacket(String command) {
    command = "!1" + command;

    byte[] buffer = new byte[command.length() + 16 + 3];
    buffer[0] = 'I';
    buffer[1] = 'S';
    buffer[2] = 'C';
    buffer[3] = 'P';
    buffer[4] = 0x00;
    buffer[5] = 0x00;
    buffer[6] = 0x00;
    buffer[7] = 0x10;
    buffer[8] = 0x00;
    buffer[9] = 0x00;
    buffer[10] = 0x00;
    buffer[11] = (byte)(command.length() + 3);
    buffer[12] = 0x01;
    buffer[13] = 0x00;
    buffer[14] = 0x00;
    buffer[15] = 0x00;
    for (int i = 0; i < command.length(); i++) {
      buffer[16 + i] = (byte)command.charAt(i);
    }
    buffer[16 + command.length()] = 10;
    buffer[17 + command.length()] = 13;
    buffer[18 + command.length()] = 10;
    
    return buffer;
  }
  
  
  public static String packetToCommand(StringBuffer packetBuffer) {
    byte[] packet = packetBuffer.toString().getBytes();
    if (packet.length <= 17) {
      return null;
    } else {
      int dataLen = packet[11] - 3;
      //System.out.println(packetBuffer.length()+" "+dataLen);
      //for (int i=0; i<packet.length; i++) System.out.print((int)packet[i]+" ");
      //System.out.println();
      //for (int i=0; i<packet.length; i++) System.out.print((char)packet[i]+" ");
      //System.out.println();
      if (16 + dataLen > packet.length) return null;
      StringBuffer data = new StringBuffer();
      for (int i = 2; i < dataLen; i++) {
        int c = packet[16 + i];
        if (c == 0) break;
        data.append((char)c);
      }
      return data.toString();
    }

  }
  
}
