import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class TestServer {

  static ServerSocket serverSocket;
  static Socket socket;
  static DummyReceiver receiver;


  public static void main(String[] args) {

    try {
      receiver = new DummyReceiver();
      serverSocket = new ServerSocket(60128);
      System.out.println("SERVER Waiting for connections...");
      socket = serverSocket.accept();

      new Thread(new Runnable() {
        public void run() {
          listen();
        }
      }).start();
      
      receiver.run();

    } catch (Exception ex) {
      ex.printStackTrace();
      return;
    }
  }


  public static void sendCommand(String command) {
    try {
      System.out.println("SERVER OUT: " + command);
      byte[] packet = CommandParser.commandToPacket(command);
      socket.getOutputStream().write(packet);
      socket.getOutputStream().flush();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }


  private static void listen() {
    try {
      InputStream input = socket.getInputStream();

      while (true) {

        StringBuffer response = new StringBuffer();
        int c = -1;
        int lastC = -1;
        while ((c = input.read()) != 10 || lastC != 13) {
          if (c == -1) {
            System.out.println("SERVER ERR: End of input stream");
            System.exit(-1);
          }
          response.append((char)c);
          lastC = c;
        }

        String command = CommandParser.packetToCommand(response);
        if (command == null) {
          System.out.println("SERVER ERR: Malformed response: " + response.toString());
        } else {
          System.out.println("SERVER IN:  " + command);
          receiver.onCommand(command);
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      System.exit(-1);
    }
  }

}
