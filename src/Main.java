import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class Main {

  static final String RECEIVER_IP = "192.168.8.71";
  //static final String RECEIVER_IP = "81.91.84.197";
  //static final String RECEIVER_IP = "localhost";

  static Socket socket;


  public static void main(String[] args) {
    if (!init())
      return;

    try {
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      while (true) {
        String line = br.readLine();
        if (line.compareTo("q") == 0)
          System.exit(0);
        sendCommand(line);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }


  private static void sendCommand(String command) {
    try {
      System.out.println("OUT: " + command);
      byte[] packet = CommandParser.commandToPacket(command);
      socket.getOutputStream().write(packet);
      socket.getOutputStream().flush();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }


  private static void listen() {
    try {
      InputStream input = socket.getInputStream();
      System.out.println("Listening...");
      
      while (true) {
        
        StringBuffer response = new StringBuffer();
        int c = -1;
        int lastC = -1;
        while ((c = input.read()) != 10 || lastC != 13) {
          response.append((char)c);
          lastC = c;
        }
        response.append(10);

        String command = CommandParser.packetToCommand(response);
        if (command == null) {
          System.out.println("ERR: Malformed response: " + response.toString());
        } else {
          System.out.println("IN:  " + command);
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      System.exit(-1);
    }
  }


  private static boolean init() {
    try {
      socket = new Socket();
      SocketAddress address = new InetSocketAddress(RECEIVER_IP, 60128);
      socket.connect(address, 1000);

      new Thread(new Runnable() {
        public void run() {
          listen();
        }
      }).start();
    } catch (Exception ex) {
      ex.printStackTrace();
      return false;
    }
    return true;
  }
}
